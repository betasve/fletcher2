ActiveAdmin.register Ticket do
	actions :all, :except => [:destroy, :new, :edit]

	index do 
		column :first_name, sortable: :first_name do |ticket|
			link_to ticket.first_name, admin_ticket_path(ticket)
		end
		column :last_name, sortable: :last_name do |ticket|
			link_to ticket.last_name, admin_ticket_path(ticket)
		end
		column :email, sortable: :email do |ticket|
			link_to ticket.email, admin_ticket_path(ticket)
		end
		column :created_at, sortable: :created_at do |ticket|
			link_to ticket.created_at, admin_ticket_path(ticket)
		end
	end

	show do |ticket|
		attributes_table do
			row :id
			row :first_name
			row :last_name
			row :birthday
			row :passport_num
			row :passport_valid_date
			row :mobile
			row :email
			row :destination
			row :departure_date
			row :comeback_date
			row :airlines
			row :arrival_time
			row :comments
			row :created_at
		end
	end

	permit_params :first_name, :last_name, :fathers_name, :birthday, :passport_num, :passport_valid_date, :mobile, :email, :destination, :departure_date, :comeback_date, :airlines, :arrival_time, :comments
end