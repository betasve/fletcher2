ActiveAdmin.register Contact do
	actions :all, :except => [:destroy, :new, :edit]

	index do 
		column :names, sortable: :names do |contact|
			link_to contact.names, admin_contact_path(contact)
		end
		column :email, sortable: :email do |contact|
			link_to contact.email, admin_contact_path(contact)
		end
	end

	show do |contact|
		attributes_table do
			row :id
			row :names
			row :email
			row :message
			row :created_at
		end
	end

	permit_params :names, :email, :message
end