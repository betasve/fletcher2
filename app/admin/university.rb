ActiveAdmin.register University do
	actions :all

	index do 
		column :id
		column :name, sortable: :name do |university|
			link_to university.name, admin_university_path(university)
		end
		column :created_at, sortable: :created_at do |university|
			link_to university.created_at, admin_university_path(university)
		end
	end

	form do |f|
	    f.inputs do
	      f.input :name
	    end
    	f.actions
  	end

	permit_params :name
end