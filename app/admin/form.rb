ActiveAdmin.register Form do
	actions :all, :except => [:destroy, :new, :edit]

	index do 
		column :first_name, sortable: :first_name do |form|
			link_to form.first_name, admin_form_path(form)
		end
		column :last_name, sortable: :last_name do |form|
			link_to form.last_name, admin_form_path(form)
		end
		column :email, sortable: :email do |form|
			link_to form.email, admin_form_path(form)
		end
		column :created_at, sortable: :created_at do |form|
			link_to form.created_at, admin_form_path(form)
		end
	end

	show do |form|
		attributes_table do
			row :id
			row :first_name
			row :fathers_name
			row :last_name
			row :birthday
			row :place_of_birth
			row :mobile
			row :skype
			row :university
			row :year_of_study
			row :email
			row :fletcher_office
			row :application_form_assistance
			row :job_offer_precheck
			row :ds_assistance
			row :created_at
		end
	end

	permit_params :last_name, :first_name, :fathers_name, :birthday, :place_of_birth, :mobile, :skype, :university, :year_of_study, :email, :fletcher_office, :application_form_assistance, :job_offer_precheck, :ds_assistance
end