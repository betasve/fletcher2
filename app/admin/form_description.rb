ActiveAdmin.register_page "Form Description" do
	page_action :save, method: :patch do
		@formd = FormDescription.first
		@formd.body_en = params[:form_description][:body_en]
		@formd.body_bg = params[:form_description][:body_bg]
		if @formd.save!
			flash[:notice] = 'Form description eddited successfully'
			redirect_to admin_form_description_path
		else
			flash[:warning] = 'Something went wrong'
			redirect_to admin_form_description_path
		end
	end

	content do
	  semantic_form_for FormDescription.first, :url => admin_form_description_save_path, :builder => ActiveAdmin::FormBuilder do |f|
	    f.inputs "Form description" do
	      f.input :body_en
	      f.input :body_bg
	    end
	    f.actions
	  end
	end
end