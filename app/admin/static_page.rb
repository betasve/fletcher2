ActiveAdmin.register StaticPage do

  #actions :all, :except => [:destroy]

  index do
    column :title_en, sortable: :title_en do |page|
      link_to page.title_en, admin_static_page_path(page)
    end
    column :title_bg, sortable: :title_bg do |page|
      link_to page.title_bg, admin_static_page_path(page)
    end
    column :slug
  end

  form do |f|
    f.inputs do
      f.input :title_en
      f.input :title_bg
      f.input :body_en, :input_html => { :class => "tinymce", :id => "body_en" }
      f.input :body_bg, :input_html => { :class => "tinymce", :id => "body_bg" }
      f.input :slug
    end
    f.actions
  end

  show do |form|
    attributes_table do
      row :id
      row :title_en
      row :title_bg
      row :body_bg do
        simple_format form.body_bg.html_safe
      end
      row :body_en do
        form.body_en.html_safe
      end
      row :slug
      row :created_at
      row :updated_at
    end
  end

  permit_params :title_en, :title_bg, :body_en, :body_bg
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
