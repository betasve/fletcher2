

function stuffForResizeAndReady(){
   // What happens on ready and resize state
   // Creating break point of when Menu Button is goin to show or hide
    if ($(window).width() < 906) {
        $("#menu").css("display", "none");
    } else {
        $("#menu").css("display", "block");
    }
}

// Toggle Menu on menuBtn click
$(document).on("ready", function(){
    $("#menuButton").on("click",function(){
        $("#menu").slideToggle();
    });
    loaded();
    $('button.close').on ('click', function(){
        $('.alert').hide();
    });
});

// Find relevant links
var loaded = function() {
    var terms = $("#terms-and-conditions");
    var terms_text = terms.text().trim();
    var apply = $("#apply-online");
    var apply_text = apply.text().trim();
    var tax = $('#tax-refund');
    var tax_text = tax.text().trim();

    $('#menu a').each(function (index){
        if (this.text.trim() == terms_text) {
            terms.attr('href', this.href);
        }

        if (this.text.trim() == apply_text) {
            apply.attr('href', this.href);
        }

        if (this.text.trim() == tax_text) {
            tax.attr('href', this.href);
        }
    });
    
    stuffForResizeAndReady();

}

$(document).on('page:load', loaded);
$(window).on("resize", stuffForResizeAndReady);