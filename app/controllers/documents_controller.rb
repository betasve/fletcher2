class DocumentsController < ApplicationController
	before_action :set_locale

	def new
		@static_pages = StaticPage.all
		@document = Document.new
	end

	def index
		@documents = Document.all
	end

	def create
		@document = Document.new params
		redirect_to admin_documents_path
	end

	def update
		@static_pages = StaticPage.all
		@document = Document.find(params[:id])

	end

	def destroy
		@document = Document.find(params[:id])
		if @document.destroy
			flash[:notice] = "Document deleted"
		else
			flash[:warning] = "Document not deleted"
		end
		redirect_to documents_path
	end

	private 

		def doc_params
			params.require(:document).permit :name, :location, :doc_type, :static_page_id
		end
	
end