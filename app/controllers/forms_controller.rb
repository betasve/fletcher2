class FormsController < ApplicationController
	before_action :set_locale

	def new
		@formd = FormDescription.first
		@universities = University.all
		@form = Form.new
	end

	def create
		@universities = University.all
		@formd = FormDescription.first
		@form = Form.new(form_params)

		if @form.valid_with_captcha?
			@form.save
			FormMailer.application_email(@form).deliver
			FormMailer.application_email_to_employee(@form).deliver
			flash[:notice] = t(:form_success)
			redirect_to root_path
		else
			render 'new'
		end
	end

	private 
	def form_params
		params.require(:form).permit(:last_name, :first_name, :fathers_name, :birthday, :place_of_birth, :mobile, :skype, :university_id, :year_of_study, :email, :fletcher_office, :application_form_assistance, :job_offer_precheck, :ds_assistance, :captcha, :captcha_key)
	end
end