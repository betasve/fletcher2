class StaticPagesController < ApplicationController
	before_action :set_locale
	def show
		@page = StaticPage.friendly.find(params[:friendly_id])
		@header = Document.where(static_page_id: @page, doc_type:"jpg").first
	end
end