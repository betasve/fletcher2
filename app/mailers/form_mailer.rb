class FormMailer < ActionMailer::Base
  default from: "lowcost@fletcherlynd.com"

  def application_email(applicant)
  	@applicant = applicant
  	mail(to: @applicant.email, subject: t(:email_subject))
  end

  def application_email_to_employee(applicant)
    @a = applicant

    if @a.fletcher_office == "Sofia"
      receiver = "info@fletcherlynd.com"
    else
      receiver = "blagoevgrad@fletcherlynd.com"
    end

    mail(to: receiver, subject: "#{@a.first_name} #{@a.last_name} попълни формата за кандидатстване в LowCostWAT")
  end

  def contact_email(contact)
    @c = contact

    mail(to: 'info@fletcherlynd.com', subject: "#{@c.names} изпрати запитване през контактната форма на LowCostWAT")
  end
end
