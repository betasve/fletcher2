class TicketMailer < ActionMailer::Base
  default from: "no-response@fletcherlynd.com"

  def ticket_email(applicant)
  	@applicant = applicant
  	mail(to: ['svetlio.blyahoff@gmail.com', 'ivo@fletcherlynd.com'], subject: "Ticket form was filled by #{@applicant.first_name} #{@applicant.last_name}") do |format|
  		format.html { render locals: { ticket: applicant } }
  		format.text { render locals: { ticket: applicant } }
  	end
  	#mail(to: 'svetlio.blyahoff@gmail.com', subject: "Successful application")
  end
end
