class Document < ActiveRecord::Base

	STORE_DIR = 'documents'
	STORE_PATH = File.join("public", STORE_DIR)
	Dir.mkdir(STORE_PATH) unless Dir.exists?(STORE_PATH)
	
	belongs_to :static_page

	validates :name, presence: true, uniqueness: true
	validates :location, presence: true, uniqueness: true
	validates :doc_type, presence: true
	validates :static_page, presence: true

	def file= file
		self.doc_type = file.original_filename.split('.').last
		File.open(File.join(STORE_PATH, file.original_filename), "wb") { |f| f.write(file.read) }
		self.location = File.join(STORE_DIR, file.original_filename)
	end
end
