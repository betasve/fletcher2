class Contact < ActiveRecord::Base

	apply_simple_captcha

	validates_each :email do |record, attr, value|
    	record.errors.add(attr, I18n.t(:invalidemail)) unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  	end

	validates :names, presence: true
	validates :message, presence: true
end
