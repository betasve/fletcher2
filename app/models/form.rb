class Form < ActiveRecord::Base
	translates :description

	apply_simple_captcha

	belongs_to :university

	validates_each :email do |record, attr, value|
    	record.errors.add(attr, I18n.t(:invalidemail)) unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  	end

  	validates_each :mobile do |record, attr, value|
    	record.errors.add(attr, I18n.t(:invalidmobile)) unless value =~ /^[0-9]{10}$/
  	end
	validates :last_name, presence: true
	validates :first_name, presence: true
	validates :fathers_name, presence: true
	validates :birthday, presence: true
	validates :place_of_birth, presence: true
	validates :mobile, presence: true
	validates :skype, presence: true
	validates :university_id, presence: true
	validates :year_of_study, presence: true
	validates :email, presence: true
	validates :fletcher_office, presence: true

end