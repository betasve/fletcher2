class Ticket < ActiveRecord::Base

	validates_each :email do |record, attr, value|
    	record.errors.add(attr, I18n.t(:invalidemail)) unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  	end

  	validates_each :mobile do |record, attr, value|
    	record.errors.add(attr, I18n.t(:invalidmobile)) unless value =~ /^[0-9]{10}$/
  	end
	validates :last_name, presence: true
	validates :first_name, presence: true
	validates :birthday, presence: true
	validates :passport_num, presence: true
	validates :passport_valid_date, presence: true
	validates :email, presence: true
	validates :mobile, presence: true
	validates :destination, presence: true
	validates :departure_date, presence: true
	validates :comeback_date, presence: true
	validates :airlines, presence: true
	validates :arrival_time, presence: true
end
