//= require active_admin/base

//= require tinymce-jquery

$(document).ready(function() {
  tinyMCE.init({
     mode: 'textareas',
     align: 'right',
     width: '80%',
     height: 200,
     autoresize_min_height: 200, 
     autoresize_max_height: 400,
     resize: 'both',
     plugins : 'advlist autolink link image lists charmap print preview uploadimage code',
     toolbar: [
                "undo redo | styleselect | bold italic | link uploadimage | alignleft aligncenter alignright | code"
                ]

   });
});