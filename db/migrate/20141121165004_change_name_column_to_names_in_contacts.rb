class ChangeNameColumnToNamesInContacts < ActiveRecord::Migration
  def change
  	rename_column :contacts, :name, :names
  end
end
