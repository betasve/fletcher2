class CreateFormDescriptions < ActiveRecord::Migration
  def change
    create_table :form_descriptions do |t|
      t.text :body_en
      t.text :body_bg

      t.timestamps
    end
  end
end
