class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :first_name
      t.string :last_name
      t.date :birthday
      t.string :passport_num
      t.date :passport_valid_date
      t.string :email
      t.string :mobile
      t.string :destination
      t.date :departure_date
      t.date :comeback_date
      t.string :airlines
      t.string :arrival_time
      t.string :comments

      t.timestamps
    end
  end
end
