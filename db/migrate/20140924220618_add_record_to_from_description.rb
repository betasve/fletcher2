class AddRecordToFromDescription < ActiveRecord::Migration
  def self.up
  	FormDescription.create!(body_en: "Sample description", body_bg: "Примерно описание")
  end

  def self.down
  	FormDescription.destroy_all(body_en: "Sample description")
  end
end
