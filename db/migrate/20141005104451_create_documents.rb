class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :name
      t.string :location
      t.string :doc_type
      t.integer :static_page_id

      t.timestamps
    end
  end
end
