class CreateForms < ActiveRecord::Migration
  def change
    create_table :forms do |t|
      t.string :first_name
      t.string :fathers_name
      t.string :last_name
      t.datetime :birthday
      t.string :place_of_birth
      t.string :mobile
      t.string :skype
      t.integer :university_id
      t.string :year_of_study
      t.string :email
      t.string :fletcher_office
      t.string :application_form_assistance
      t.string :job_offer_precheck
      t.string :ds_assistance

      t.timestamps
    end
  end
end
